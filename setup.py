from setuptools import setup, find_packages

setup(
    name='iot_ngin_ssi_tools',
    version='0.1',
    description=(
        'Tools for creating Verificable Credentials and DPoP proofs in JWT format'
    ),
    url='https://gitlab.com/h2020-iot-ngin/enhancing_iot_cybersecurity_and_data_privacy/privacy-preserving-self-sovereign-identities',
    author='IoT-NGIN Project',
    license='APL 2.0',
    packages=['ssi_tools'],
    install_requires=[
        'jwcrypto',
        'cryptography',
        'base58',
        'jsonpath-ng',
        'Werkzeug',
        'requests'
    ],
    tests_require=['pytest', 'pytest-asyncio'],
    zip_safe=False
)
