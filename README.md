# Privacy preserving Self-Sovereign Identities

## Description

This repository provides tools for managing and using Self-Sovereign Identities (SSIs) in the Internet of Things (IoT) setting.

![SSI](figures/SSI1.png)
*Figure 1: Overview of the usage of SSI component*

Figure 1 provides an overview how the SSI component can be used to grant and verify access to the Resource Server, which can be for example an IoT device. The Resource Owner and Client are identified using [Decentralized Identifiers (DIDs)](https://www.w3.org/TR/did-core/), in the first step the Owner configures the IAA proxy and grants a [Verifiable Credential (VC)](https://www.w3.org/TR/vc-data-model) to the Client, which denotes that the Client has a right to access some Resource. The Client uses this credential to contact the IAA proxy or the actual IoT device, which will then verify the credential and grant a read or write access to the resource. In a case of the IAA proxy, it will forward the request to the actual Resource Server, which does not need to understand SSI technologies or even handle the cryptographic operations.

In more detail, the credential is encoded as a standard [JSON Web Token (JWT)](https://datatracker.ietf.org/doc/html/rfc7519) and in order to prevent replay attacks, the client also constructs a [Demonstrating of Proof-of-Possession (DPoP)](https://datatracker.ietf.org/doc/html/draft-ietf-oauth-dpop) proof when accessing the resource. Both the credential and the DPoP proof will be verified by the IAA proxy or the actual device.

The SSI component provides the following functionality:
1. Tools for identity and key management, including the creation of credentials encoded as JWTs and DPoP proofs. For DID methods *did:self* and *did:key* are supported and the Ed25519 EdDSA signature scheme is supported for cryptoraphic signatures.

2. IAA proxy and simple resource server based on Nikos Fotiou's [py-verifier](https://github.com/mmlab-aueb/py-verifier) work.

3. [Verifier for ESP32-based embedded devices](esp32-verifier/).



## Usage


### Prerequisites

**Python 3.8**, on some platforms a Rust compiler may be necessary to compile necessary Python modules.

**curl** for running the example.

### Installation

The dependencies can be installed with the following commands (note that it is also possible to run the component using a docker image as described in the [Docker Images](#Docker-Images) section):

```bash
python3 setup.py develop # Install project dependencies locally
```

The following commands also apply.

```
python3 setup.py build
python3 setup.py install
```

### Configuration

The configuration file of the IAA proxy ([py-verifier/conf/iaa.conf](py-verifier/conf/iaa.conf)) contains information about trusted Resource Owners and service endpoints. Within the file, "resources" attribute contains all service endpoints, and under each endpoint the "authorization" attribute contains the authorization type (should be "jwt-vc-dpop"), a public key or DID information about the trusted issuer (Resource Owner), and optionally filter for e.g. valid capabilities of the credential necessary to access the resource.

One can use the default configuration, or deploy a different key as follows:

```bash
# generate new key for the owner
python3 key_tools.py generate_key owner.pem

# show owner's DID
python3 key_tools.py show_did owner.pem
```

Afterwards, the Owner's DID should be added to the configuration file. Note, DID is present twice per entry, directly under "trusted_issuers" and also as a value for "issuer_key" attribute, also the different DIDs must be present separetely in the configuration, even if they are derived from the same public key. Furthermore, the "filters" attribute also contains required credential capabilities. Finally, the "proxy_pass" attribute refers to the resource server address to which the request will be forwarded.
```
{
  "resources": {
    "/sensor1": {
      "authorization": {
        "type":"jwt-vc-dpop",
        "trusted_issuers": {
          "did:key:z6Mkk4YdpLxAxWkDULdBVifCjVDPh3WvhbkDL1W4miwoHEQb": {
            "issuer_key":"did:key:z6Mkk4YdpLxAxWkDULdBVifCjVDPh3WvhbkDL1W4miwoHEQb",
            "issuer_key_type": "did"
          },
          "did:self:U1SFfNLQWY6g1-n1RB_Qe_4FTMTght0woT-QOcdR9ow": {
            "issuer_key":"did:self:U1SFfNLQWY6g1-n1RB_Qe_4FTMTght0woT-QOcdR9ow",
            "issuer_key_type": "did"
          }
        },
        "filters": [
          ["$.vc.credentialSubject.capabilities.'https://devices.iot-ngin.eu/sensor1'[*]", "READ"]
        ]
      },
      "proxy": {
        "proxy_pass": "http://localhost:8080"
      }
    }
    ...
  }
}

```

In order to access this resource, a credential with the corresponding capability should be used ([example](credentials/example-vc.json)):
```
....
"credentialSubject": {
  "capabilities": {
    "https://devices.iot-ngin.eu/sensor1": ["READ"],
    "https://devices.iot-ngin.eu/sensor2": ["READ"],
    "https://devices.iot-ngin.eu/lamp1":  ["READ", "WRITE"]
  } 
} 
```
This credential has the "READ" capability for the URL "https://devices.iot-ngin.eu/sensor1" and therefore matches with the configuration file filter presented above. The exact format of the capabilities can be chosen freely and also other fields of the JWT-encoded credential can be matched within the filter.

The IAA and the example resource server can be configured with the following environmental variables:

`IAA_CONF_FILE` the path to the IAA configuration file (default: `conf/iaa.conf`)

`IAA_ADDRESS` the IP address used by IAA (default: `localhost`)

`IAA_PORT`the port used by IAA (default: `9000`)

`HTTP_SERVER_ADDRESS` the IP address used by the resource server (default: `localhost`)

`HTTP_PORT` the port used by the resource server (default: `8080`)


### Execution

The component provides the following functionality:

1. Create a private key and store it to a file in PEM format.
```bash
python3 key_tools.py generate_key keyfile.pem
```


2. Display DID associated with the private key in *did:self* (default) *did:key* format.
```bash
python3 key_tools.py show_did --did_method did:key keyfile.pem
```


3. Generate and display Verifiable Credential (VC) from the issuer to the subject encoded as JWT. The second parameter is subject's DID, while the third is the VC template file ([example](credentials/example-vc.json)). Optional parameters are DID method to use for the issuer (default: *did:self*) and validity time of the credential in seconds (default 1 year).

```bash
python3 create_jwt.py issuer.pem did:key:z6Mkt7WusEVCRQPzjWjAgeYHKw7RLZ4CpkGknDALtgjMCTn7 credentials/example-vc.json --did_method did:key --validity_time 86400
```


4. Generate and display DPoP based on JWT-VC signed by the user's private key. The second parameter is HTTP method to use for the resource request ("htm" claim of DPoP), third one is HTTP URI of the request ("htu" claim of DPoP), and the final one is JWT-VC created in step 3.
```bash
python3 create_dpop.py keyfile.pem GET https://devices.iot-ngin.eu/sensor1 JWT_TOKEN
```


After the JWT and DPoP proof have been created, the resource can be accessed by including the following HTTP headers in the HTTP request. Here JWT is a JWT created in step 3 and DPOP is a DPoP proof created in step 4:
```
Authorization: DPoP JWT
dpop: DPOP
```

### Example

[ssi-example](ssi-example.sh) script is provided to automate above mentioned steps, it can be used together with the `py-verifier` in the following way:

```bash
# start the example resource server on one terminal
python3 py-verifier/tests/http_server.py

# start IAA part of py-verifier on second terminal
cd py-verifier && python3 IAA/iaa.py

# on third terminal, execute the following command
bash ssi-example.sh keys/owner.pem keys/client.pem credentials/example-vc.json http://127.0.0.1:9000/sensor1

# alternatively, did:key method can be used for issuer and subject in the JWT-VC:
bash ssi-example.sh keys/owner.pem keys/client.pem credentials/example-vc.json http://127.0.0.1:9000/sensor1 -did:key

```



New keys for the resource owner and user can be generated as mentioned previously:
```bash
# generate necessary keys
python3 key_tools.py generate_key new_owner.pem
python3 key_tools.py generate_key new_client.pem

# show owner's DID
python3 key_tools.py show_did new_owner.pem
```

Afterwards the `py-verifier/conf/iaa.conf` needs to be modified to include the new DID under "trusted_issuers".  Then the first three steps of the example can be run.


### Docker Images

The Docker container can be build using command: `docker build .`.

## Testing

## Open Issues and Limitations

- Only the Ed25519 EdDSA signature scheme is supported for cryptographic operations.


## Future Work

## Release Notes


## Contact Information

Contact: Dmitrij Lagutin (dmitrij.lagutin _at_ aalto.fi)

For questions related to `py-verifier` please contact Nikos Fotiou.


## License

This component is licensed under the Apache License 2.0.
