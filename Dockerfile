FROM python:3.6

RUN apt-get update && \
    apt-get -y install gcc g++ linux-headers-amd64 libffi-dev libgnutls28-dev && \
	rm -rf /var/lib/apt/lists/*
COPY ./ /var/ssi/
WORKDIR /var/ssi
RUN python3 setup.py develop
        
