from jwcrypto.common import base64url_decode, base64url_encode
from jwcrypto import jwk, jws
import json, sys, base58, argparse

if __name__ == "__main__":
    """
    Utilities for managing private keys and DIDs
    
    generate_key: Generates Ed25519 keypair and stores a private key in PEM file
    
    show_did: Reads keypair from a PEM file and displays DID in did:self (default)
        or did:key format
    """

    parser = argparse.ArgumentParser(usage='key_tools.py [command] [options]')
    sub_parsers = parser.add_subparsers(title='Available commands', metavar='', dest='command')
    parser_generate_key = sub_parsers.add_parser('generate_key', 
                                                 help='Generates Ed25519 keypair')
    parser_generate_key.add_argument('private_key_file', type=str,
                                     help='PEM file for storing the private key')

    parser_show_did = sub_parsers.add_parser('show_did',
                                             help='Displays DID in did:self (default) or did:key format')
    parser_show_did.add_argument('private_key_file', type=str,
                                 help='PEM file for reading the private key associated with DID')
    parser_show_did.add_argument('--did_method', type=str,
                                 help='DID method to use: did:self or did:self')
    args = parser.parse_args()
    

    if args.command == None: # show help
        parser.print_help()

    elif args.command == 'generate_key':
        key = jwk.JWK.generate(kty='OKP', crv='Ed25519')
    
        # write private key to a file in PEM format
        with open(args.private_key_file, "wb") as private_key_file:
            private_key_file.write(key.export_to_pem(True, None))
            private_key_file.close()

    elif args.command == 'show_did':
        
        if (args.did_method != None) and (args.did_method != "did:self") and (args.did_method != "did:key"):
            print("Error: Invalid DID method:", args.did_method)
            exit()
        
        # read private key from file and calculate DID
        with open(args.private_key_file, "rb") as private_key_file:
            data = private_key_file.read()
            key = jwk.JWK.from_pem(data)
            private_key_file.close()
            
            key_data = key.export(as_dict=True)
            
            if args.did_method == "did:key":
                did = "did:key:z" + base58.b58encode(b'\xed\x01' + base64url_decode(key_data['x'])).decode()
            else:
                did = "did:self:" + key_data['x']
                
            print(did)
