#include <WiFi.h>
#include <ESPAsyncWebServer.h>

#include "config.h"
#include "utils.h"
#include "jwt.h"


AsyncWebServer server(80);

void setup() {
    // put your setup code here, to run once:
    Serial.begin(115200);

    if (!WiFi.softAP(WIFI_SSID, WIFI_PASSWORD)) {
        Serial.println("Starting WiFI AP failed");
    }

    delay(100); // hack to make AP configuration to work
    if (!WiFi.softAPConfig(IPAddress(IP), IPAddress(GATEWAY), IPAddress(SUBNET))) {
        Serial.println("WiFi AP Config Failed");
    }

    Serial.println(WiFi.softAPIP());

    server.on("/sensor1", HTTP_GET, jwt::verify_request);
    server.on("/sensor2", HTTP_GET, jwt::verify_request);
    server.on("/lamp1", HTTP_GET, jwt::verify_request);
    server.on("/lamp1", HTTP_POST, jwt::verify_request);

    server.begin();
}

void loop() {
    // put your main code here, to run repeatedly:

}
